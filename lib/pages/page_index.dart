import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '내 사랑 뽀삐',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
                'assets/cat1.jpeg',
              width: 400,
              height: 280,
              fit: BoxFit.cover,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                      '뽀삐를 소개합니다.',
                    style: TextStyle(
                      letterSpacing: 2.0,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text('뽀삐는 귀여워요. 하지만 하찮아요'),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                          'assets/cat2.jpeg',
                          width: 200,
                          height: 150,
                          fit: BoxFit.cover,
                      ),
                      Image.asset(
                          'assets/cat3.jpg',
                          width: 200,
                          height: 150,
                          fit: BoxFit.cover,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                          'assets/cat4.jpg',
                          width: 200,
                          height: 150,
                          fit: BoxFit.cover,
                      ),
                      Image.asset(
                          'assets/cat5.jpg',
                          width: 200,
                          height: 150,
                          fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

